# Description

The primorum library provides fast compression and decompression of a list of primes, using a compact representation.

# Licensing

The primorum library is free software and is available to be redistributed and/or
modified under the terms of either the GNU Lesser General Public
License (LGPL) version 3.0.
