import os
import sys
import subprocess


if __name__ != '__main__':
    sys.exit()
if len(sys.argv) != 5:
    raise SystemExit('Usage: {} PATH1 PATH2 BUILDDIR MAXN'.format(sys.argv[0]))
pdir = os.path.join(sys.argv[3], 'primes')
os.makedirs(pdir, exist_ok=True)
for root, dirs, files in os.walk(pdir):
    for f in files:
        os.remove(os.path.join(root, f))
generator = subprocess.Popen([sys.argv[1], sys.argv[4]], stdout=subprocess.PIPE)
compressor = subprocess.Popen([sys.argv[2], os.path.join(pdir, 'primes.bin'),
    sys.argv[4]], stdin=generator.stdout, stdout=sys.stdout)
generator.wait()
if generator.returncode != 0:
    raise subprocess.CalledProcessError(generator.returncode, sys.argv[1],
        generator.stdout, generator.stderr)
compressor.wait()
if compressor.returncode != 0:
    raise subprocess.CalledProcessError(compressor.returncode, sys.argv[2],
        compressor.stdout, compressor.stderr)
