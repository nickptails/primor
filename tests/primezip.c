#include "stdlib.h"
#include "stdio.h"
#include "stdbool.h"
#include "unistd.h"
#include "string.h"
#include "sys/mman.h"
#include "sys/stat.h"
#include "sys/types.h"
#include "fcntl.h"
#include "primorum.h"

bool write_prime(unsigned long prime, int pfd, unsigned char** plist, off_t* fsize)
{
    const size_t pagesize = sysconf(_SC_PAGE_SIZE);
    off_t newsize = (prime / 30 + 1 + pagesize) & ~(pagesize - 1);
    unsigned char* fmap = NULL;
    if (newsize > *fsize) {
        if (ftruncate(pfd, newsize) == -1) {
            fprintf(stderr, "Unable to resize file..\n");
            return false;
        }
        if (*plist == NULL)
            fmap = mmap(NULL, newsize, PROT_READ | PROT_WRITE, MAP_SHARED, pfd, 0);
        else
            fmap = mremap(*plist, *fsize, newsize, MREMAP_MAYMOVE);
        if (fmap == NULL) {
            fprintf(stderr, "Unable to map file..\n");
            return false;
        }
        *plist = fmap;
        *fsize = newsize;
    }
    primorum_pack(*plist + prime / 30, prime % 30);
    return true;
}

bool read_num(char* line, unsigned long* num)
{
    char* endstr;
    *num = strtol(line, &endstr, 0);
    if (*endstr != '\n' && *endstr != '\0') {
        fprintf(stderr, "Invalid number: %s\n", line);
        return false;
    }
    return true;
}

void read_primes(int pfd)
{
    unsigned char ppack;
    unsigned char unpacked[PRIMORUM_PACK_LENGTH];
    int biter;
    unsigned long primebase = 0;
    while (read(pfd, &ppack, sizeof(unsigned char)) > 0) {
        memset(unpacked, 0, PRIMORUM_PACK_LENGTH * sizeof(unsigned char));
        primorum_unpack(&ppack, unpacked);
        for (biter = 0; biter < PRIMORUM_PACK_LENGTH; biter++)
            if (unpacked[biter] != 0)
                printf("%lu\n", primebase + unpacked[biter]);
        primebase += 30 * sizeof(unsigned char);
    }
}

#define PRIME_DELIM ' '


int main(int argc, char* argv[])
{
    int pfd;
    unsigned long maxn = 0, pprime = 0;
    char* linebuf = NULL;
    size_t bufsize = 0;
    char* endstr;
    ssize_t delout;
    unsigned char* pmap = NULL;
    off_t pmaplen = 0;
    if (argc != 3) {
        fprintf(stderr, "Usage: %s FILE MAXN\n", argv[0]);
        return EXIT_FAILURE;
    }
    pfd = open(argv[1], O_CREAT | O_EXCL | O_RDWR, 0644);
    if (pfd == -1) {
        fprintf(stderr, "Unable to create file: %s\n", argv[1]);
        return EXIT_FAILURE;
    }
    maxn = strtol(argv[2], &endstr, 0);
    if (*argv[2] == '\0' || *endstr != '\0') {
        fprintf(stderr, "Invalid argument for maximum number: %s\n", argv[2]);
        return EXIT_FAILURE;
    }
    delout = getline(&linebuf, &bufsize, stdin);
    while (delout > 0) {
        if (!read_num(linebuf, &pprime))
            continue;
        if (pprime < maxn && !write_prime(pprime, pfd, &pmap, &pmaplen)) {
            fprintf(stderr, "Unable to write number: %lu\n", pprime);
            return EXIT_FAILURE;
        }
        delout = getline(&linebuf, &bufsize, stdin);
    }
    free(linebuf);
    if (munmap(pmap, pmaplen) == -1) {
        fprintf(stderr, "Error while unmapping file..\n");
        return EXIT_FAILURE;
    }
    if (!feof(stdin)) {
        fprintf(stderr, "Error while reading line..\n");
        return EXIT_FAILURE;
    }
    if (lseek(pfd, 0, SEEK_SET) == -1) {
        fprintf(stderr, "Cannot get back to the beginning of the file..\n");
        return EXIT_FAILURE;
    }
    read_primes(pfd);
    if (close(pfd) == -1) {
        fprintf(stderr, "Error while closing file..\n");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
