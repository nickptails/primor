#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "sys/types.h"
#include "primorum.h"

const unsigned char inc_array[] = {6, 4, 2, 4, 2, 4, 6, 2};


void primorum_unpack(unsigned char* pkbyte, unsigned char unpked[PRIMORUM_PACK_LENGTH])
{
    int pkiter;
    unsigned char pprime = 31, pbyte = *pkbyte;
    for (pkiter = PRIMORUM_PACK_LENGTH - 1; pkiter >=0; pkiter--) {
        pprime -= inc_array[pkiter];
        if (pbyte & 1)
            unpked[pkiter] = pprime;
        pbyte >>= 1;
    }
}

void primorum_pack(unsigned char* pkbyte, unsigned char prime)
{
    int pkiter;
    unsigned char pprime = 1;
    for (pkiter = 0; pkiter < PRIMORUM_PACK_LENGTH; pkiter++) {
        if (pprime >= prime)
            break;
        pprime += inc_array[pkiter];
    }
    if (pkiter < PRIMORUM_PACK_LENGTH && pprime == prime)
        *pkbyte |= 1 << (PRIMORUM_PACK_LENGTH - 1 - pkiter);
}
