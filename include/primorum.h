/** @file primorum.h
 *  @brief Utilities for list of primes.
 *  
 *  Collection of functions for reading (writing) a list of primes from (to) a
 *  compact memory representation.
 */

#ifndef PRIMORUM_H
#define PRIMORUM_H

#define PRIMORUM_PACK_LENGTH 8

void primorum_unpack(unsigned char* pkbyte, unsigned char unpked[PRIMORUM_PACK_LENGTH]);

void primorum_pack(unsigned char* pkbyte, unsigned char prime);

#endif
